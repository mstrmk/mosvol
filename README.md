# Сервис по размещению и поиску задач для волонтеров

### Стек

Backend: Typescript, node.js, express.js
Database: PostgreSQL
Frontend: Javascript, React.js, Redux, Router5, VK UI, HTML, sass

### Репозитории
Frontend: [https://gitlab.com/mstrmk/mosvol](https://gitlab.com/mstrmk/mosvol)
Backend: [https://gitlab.com/kwels/volunteer](https://gitlab.com/kwels/volunteer)

### Ссылка на приложение
https://vk.com/app8191012