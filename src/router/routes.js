export default [
    { name: 'main', path: '/' },
    { name: 'reg', path: '/reg' },
    { name: 'events', path: '/events' },
    { name: 'event', path: '/events/:id' },
    { name: 'profile', path: '/profile' },
    { name: 'store', path: '/store' },
    { name: 'myevents', path: '/myevents' },
    { name: 'createMyevent', path: '/myevents/create' },
    { name: 'botSettings', path: '/bot_settings' },
]