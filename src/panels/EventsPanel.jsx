import React, { useState, useEffect } from 'react';
import {connect} from 'react-redux';
import { Panel, PanelHeader, IconButton, Button, Group, Input, FormItem, Title, Spinner } from '@vkontakte/vkui';
import { Icon20FilterOutline } from '@vkontakte/icons';

import EventCard from 'components/EventCard';
import EventRequestModal from 'components/modals/EventRequestModal';

import api from 'utils/api';

const EventsPanel = ({id, router}) => {
    
    const [filter, setFilter] = useState({
        q: '',
    });
    const [applyFilterFlag, setApplyFilterFlag] = useState(false);

    const setFilterValue = (name, value) => {
        setForm({...form, [name]: value });
    }

    const onChangeFilter = e => {
        const { name, value } = e.currentTarget;
        setFilterValue(name, value);
    }

    const showFilter = () => {
        //
    };

    const [events, setEvents] = useState([]);
    const [loading, setLoading] = useState(true);

    const fetchEvents = () => {
        setLoading(true);
        api.event.fetch().ok(data => {
            setEvents(data);
            setLoading(false);
        })
        .send();
    };

    useEffect(() => {
        fetchEvents();
    }, [applyFilterFlag]);

    const open = id => {
        router.navigate('event', {id});
    };

    const [isRequestModalShow, setIsRequestModalShow] = useState(false);
    const [rqModalEventId, setRqModalEventId] = useState(false);

    const sendRequest = id => {
        setRqModalEventId(id);
        setIsRequestModalShow(true);
    };

    const [requestedEvents, setRequestedEvents] = useState([]);

    const handleRequested = eventId => {
        setRequestedEvents(requestedEvents.concat([eventId]));
    };

    return (
        <Panel id={id}>
            <PanelHeader>Столичный добряк</PanelHeader>
            {/* <FormItem> 
                <Input type="text" name="q" value = { filter.q } onChange = { onChangeFilter } 
                after={
                    <IconButton hoverMode="opacity" aria-label="Открыть фильтр" onClick={showFilter}>
                        <Icon20FilterOutline />
                    </IconButton> 
                } />
            </FormItem> */}
            <Group mode="plain" style = {{padding: '0px 16px', marginTop: 8}}>
                <Title level="1" style={{ marginBottom: 24 }}>Доска заданий</Title>
                {loading ? <Spinner size="large" style={{ marginTop: '40px' }} /> : (events.length ? events.map(event => <EventCard event = { event } key = { event.id } open = { open } sendRequest = { sendRequest } isRequested = { requestedEvents.includes(event.id) } />) : 'Заданий не найдено')}
            </Group>
            <EventRequestModal isShow = { isRequestModalShow } setShow = { setIsRequestModalShow } eventId = { rqModalEventId } handleRequested = { handleRequested } />
        </Panel>
    );
}

export default connect(() => ({}))(EventsPanel);