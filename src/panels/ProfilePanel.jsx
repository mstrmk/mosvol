import React, { useEffect } from 'react';
import {connect} from 'react-redux';
import { Panel, PanelHeader, Button, RichCell, Group, Avatar, Caption, Title, Header, SimpleCell } from '@vkontakte/vkui';
import bridge from "@vkontakte/vk-bridge";

import { Icon28EditOutline, Icon56ServicesOutline, Icon24RobotOutline } from '@vkontakte/icons';

import ListBadges from 'components/ListBadges';

import moment from 'moment';
import numberHelper from 'utils/helpers/number';


const ProfilePanel = ({id, router, profile}) => {
    
    const age = profile.birthday ? Math.floor(moment().diff(moment(profile.birthday), 'years', true)) : null;

    return (
        <Panel id={id}>
            <PanelHeader>Столичный добряк</PanelHeader>
            <Group mode="plain" style = {{padding: '0px 16px', marginTop: 8, marginBottom: 24}}>
                <Title level="1">Профиль</Title>
            </Group>
            <div style={{ marginBottom: 24 }}>
                <RichCell
                    disabled
                    multiline
                    before={<Avatar size={48} src="https://sun9-69.userapi.com/s/v1/if1/4if1sU4rN2ZWSWQv1SZsqgrjVyC0acEEdB7VH3up4c2U2JfBUohjFiaACWFiKDwpZBNxGPGG.jpg?size=700x700&quality=96&type=album" />}
                    caption={age ? numberHelper.skl(age, ['год', 'года', 'лет'], true) : ''}
                >
                    {`${profile.firstName} ${profile.lastName}`}
                </RichCell>
            </div>
            {/* <SimpleCell
              onClick={() => {}}
              expandable
              before={<Icon28EditOutline width = {24} height = {24} />}
            >
              Редактировать
            </SimpleCell> */}
            <SimpleCell
              onClick = {e => {router.navigate('myevents');}}
              expandable
              before={<Icon56ServicesOutline width = {24} height = {24} />}
            >
              Задания от меня
            </SimpleCell>
            <SimpleCell
              onClick={e => {router.navigate('botSettings');}}
              expandable
              before={<Icon24RobotOutline width = {24} height = {24} />}
            >
              Настройки рассылки
            </SimpleCell>
        </Panel>
    );
}

export default connect(({ user }) => ({ profile: user.profile }))(ProfilePanel);