import React, { useEffect, useState } from 'react';
import {connect} from 'react-redux';
import { Panel, PanelHeader, Spinner, Group, Title, Snackbar, Avatar } from '@vkontakte/vkui';
import { Icon16Done } from '@vkontakte/icons';
import bridge from "@vkontakte/vk-bridge";

import api from 'utils/api';
import StoreGoodCard from '../components/StoreGoodCard';
import CoinBadge from '../components/CoinBadge';

import { setProfile } from 'store/user/actions'; 

const MainPanel = ({id, router, profile, dispatch}) => {

    const [goods, setGoods] = useState([]);
    const [loading, setLoading] = useState(false);
    const [promos, setPromos] = useState([]);

    useEffect(() => {
        setLoading(true);
        api.store.fetchGoods().ok(data => {
            setGoods(data);
            setLoading(false);
        })
        .catch(() => {
            setGoods([]);
        })
        .send();
    }, []);

    const buyPromo = (id, cost) => {
        api.store.buyGood(id).created(data => {
            const code = data.code;
            setPromos(promos.concat([{id, code}]));
            dispatch(setProfile({...profile, balance: profile.balance - cost}));
        })
        .catch(() => {
        })
        .send();
    };

    const [isCopy, setIsCopy] = useState(false);

    const copy = (promo) => {
        !isCopy && setIsCopy(true);
    };

    return (
        <Panel id={id}>
            <PanelHeader>Столичный добряк</PanelHeader>
            <Group mode="plain" style = {{padding: '0px 16px', marginTop: 8}}>
                <Title level="1" style={{ marginBottom: 24 }}>Доступные бонусы</Title>
                <div style = {{marginBottom: 24}}>У вас <CoinBadge count = { profile.balance }/> баллов</div>
                {!loading ? (goods.length ? goods.map(good => <StoreGoodCard key = { good.id } good = { good } promo = { promos.find(item => item.id === good.id && item.code)?.code } buyPromo = { buyPromo } copy = { copy } />) : 'Предложений не найдено') : <Spinner size="large" style={{ marginTop: '40px' }} />}
            </Group>
            {isCopy && <Snackbar
            onClose={() => setIsCopy(false)}
            before={
                <Avatar size={24} style={{ background: "var(--accent)" }}>
                <Icon16Done fill="#fff" width={14} height={14} />
                </Avatar>
            }
            >
            Промокод скопирован
            </Snackbar>}
        </Panel>
    );
}

export default connect(({ user }) => ({ profile: user.profile }))(MainPanel);