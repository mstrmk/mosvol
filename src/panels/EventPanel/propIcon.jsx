import { Caption } from '@vkontakte/vkui';

import './style.scss';

const EventPropIcon = ({icon, children}) => {
    return <div className = "event__propIconWrapper">
        <div className="event__propIcon">
            {icon}
        </div>
        <div className="event__propIconValue">
            {children}
        </div>
    </div>
};

export default EventPropIcon;