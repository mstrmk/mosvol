import { Caption } from '@vkontakte/vkui';

import './style.scss';

const EventProp = ({label, children}) => {
    return <div className = "event__prop">
        <Caption level="1" className = "event__propLabel">{label}</Caption>
        {children}
    </div>
};

export default EventProp;