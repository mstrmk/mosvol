import React, { useEffect, useState } from 'react';
import { Panel, PanelHeader, PanelHeaderBack,  Caption, Title, Button, ModalRoot, ModalCard, FixedLayout } from '@vkontakte/vkui';
import { Icon20CalendarOutline, Icon20PlaceOutline, Icon20Users3Outline, Icon20CheckCircleOutline } from '@vkontakte/icons';
import ListBadges from 'components/ListBadges';
import ListMetroStantions from 'components/ListMetroStantions';
import TextMore from 'components/TextMore';

import './style.scss';
import EventProp from './prop';
import EventPropIcon from './propIcon';

import api from 'utils/api';
import numberHelper from 'utils/helpers/number';
import EventRequestModal from 'components/modals/EventRequestModal';

import moment from 'moment';

const EventPanel = ({id, router}) => {

    const eventId = router.getState().params.id;
    const [event, setEvent] = useState(null);

    useEffect(() => {
        api.event.fetchOne(eventId).ok(data => {setEvent(data)}).send();
    }, []);

    const [isModalShow, setIsModalShow] = useState(false);

    const handleRequested = eventId => {
        setEvent({...event, isRequested: true});
    };

    const buildFormatDate = date => `${moment(date.day).format("D MMMM")} ${date.timeStart.slice(0, -3)} - ${date.timeEnd.slice(0, -3)}`;
    const dates = (dates => dates.map(date => buildFormatDate(date)))(event ? event.eventDays : []);


    return (
       <Panel id={id}>
            <PanelHeader before={<PanelHeaderBack onClick={e => router.navigate('events')} />}>{event ? event.name : ''}</PanelHeader>
            {event ? <>
                <div className = "event">
                    <div className = "event__image">
                        <img src = {event.photo ?? 'https://sun9-21.userapi.com/s/v1/if2/PdU-MUpmivYgoa1Fdsq2pWkxW_MM250msv5-O9xdFELmlFHqFxRyybVPH-zlsUP9C0_JacwIMvRSik0FD0k01hdk.jpg?size=899x660&quality=95&type=album'}/>
                    </div>
                    <div className = "event__content">
                        <Caption className = "event__company" level="1">{event.organization ? event.organization.name : (event.user ? `${event.user.firstName} ${event.user.lastName.slice(0, 1)}.` : '')}</Caption>
                        <Title level="1" className = "event__name">
                            {event.name}
                        </Title>
                        <div className = "event_propIcons">
                            {dates.length ? <EventPropIcon icon = { <Icon20CalendarOutline /> }>{dates.map((date, index) => <div key = {index}>{date}</div>)}</EventPropIcon> : ''}
                            {event.address ? <EventPropIcon icon = { <Icon20PlaceOutline /> }>{event.address}</EventPropIcon> : ''}
                            <EventPropIcon icon = { <Icon20Users3Outline /> }>{event.countPeople ? numberHelper.skl(event.countPeople, ['участник', 'участника', 'участников'], true) : 'Пока нет участников'}</EventPropIcon>
                        </div>
                        {event.description ? <EventProp label = "Описание">
                            <TextMore text = { event.description } maxLength = { 200 }/>
                        </EventProp> : ''}
                        {event.responsibilities ? <EventProp label = "Что нужно делать">
                            <TextMore text = { event.responsibilities } maxLength = { 200 }/>
                        </EventProp> : ''}
                        {event.reward ? <EventProp label = "Вознаграждение для Волонтеров">
                            <TextMore text = { event.reward } maxLength = { 200 }/>
                        </EventProp>: ''}
                        {event.claims ? <EventProp label = "Требования">
                            <TextMore text = { event.claims } maxLength = { 200 }/>
                        </EventProp> : ''}
                        {event.categories.length ? <EventProp label = "Категория">
                            <ListBadges badges = { event.categories.map(c => c.name) } />
                        </EventProp> : ''}
                        {event.stations.length ? <EventProp label = "Ближайшие станции метро">
                            <ListMetroStantions stantions = { event.stations } />
                        </EventProp> : ''}
                        <FixedLayout vertical="bottom" style = {{left: 16, right: 16}}>
                        <div className="event__actions">
                            {!event.isRequested ? <Button appearance='accent' size='l' stretched onClick = {e => { setIsModalShow(true) }}>Записаться волонтером</Button> : <div className = "event__rqSuccess"><Icon20CheckCircleOutline/><span>Вы участвуете</span></div>}
                        </div>
                        </FixedLayout>
                    </div>
                </div>
                <EventRequestModal isShow = { isModalShow } setShow = { setIsModalShow } eventId = { event.id } handleRequested = { handleRequested } />
                </> : ''}
        </Panel>
    );
}

export default EventPanel;