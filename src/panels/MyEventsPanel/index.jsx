import React, { useState, useEffect } from 'react';
import { Panel, PanelHeader, Title, Group, Spinner, Button } from '@vkontakte/vkui';

import './style.scss';

import api from 'utils/api';

import MyEventCard from 'components/MyEventCard';

const MyEventsPanel = ({id, router}) => {
    const [events, setEvents] = useState([]);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        api.event.fetchMy().ok(data => {
            setEvents(data);
        }).send();
    });

    return <Panel id={id}>
    <PanelHeader>Столичный добряк</PanelHeader>
        <Group mode="plain" style = {{padding: '0px 16px', marginTop: 8}}>
            <Title level="1" style={{ marginBottom: 24 }}>Задания от Вас</Title>
            <Button size = 'm' onClick = { e => {router.navigate('createMyevent')} }>Создать новое задание</Button>
            <div style = {{marginTop: 24}}>
            {loading ? <Spinner size="large" style={{ marginTop: '40px' }} /> : (events.length ? events.map(event => <MyEventCard event = { event } key = { event.id } open = { id => { router.navigate('event', {id}); } } />) : 'Заданий не найдено')}
            </div>
        </Group>
    </Panel>
};

export default MyEventsPanel;