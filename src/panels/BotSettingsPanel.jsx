import React, { useEffect, useState } from 'react';
import {connect} from 'react-redux';
import { Panel, PanelHeader, Button, RadioGroup, Radio, FormItem, Title } from '@vkontakte/vkui';

const BotSettingsPanel = ({id, router}) => {
    
    const save = e => {
        router.navigate('profile');
    }

    const [days, setDays] = useState(7);

    return (
        <Panel id={id}>
            <PanelHeader>Столичный добряк</PanelHeader>
            <FormItem><Title>Настройка бота</Title></FormItem>
            <FormItem top="Частостка рассылки">
                <RadioGroup>
                    <Radio name="days" value="1" onChange = { e => { setDays(1) } } checked = { days == '1' }>Раз в день</Radio>
                    <Radio name="days" value="7" onChange = { e => { setDays(7) } } checked = { days == '7' }>Раз в неделю</Radio>
                    <Radio name="days" value="30" onChange = { e => { setDays(30) } } checked = { days == '30' }>Раз в месяц</Radio>
                    <Radio name="days" value="0" onChange = { e => { setDays(0) } } checked = { days == '0' }>Никогда</Radio>
                </RadioGroup>
            </FormItem>
            <FormItem>
                <Button size="l" stretched onClick={ save }>Сохранить</Button>
            </FormItem>
        </Panel>
    );
}

export default connect(({}) => ({}))(BotSettingsPanel);