import React, { useState, useEffect } from 'react';
import {connect} from 'react-redux';
import { Panel, PanelHeader, PanelHeaderBack, Group, FormLayout, FormItem, Input, Select, DatePicker, RadioGroup, Radio, Button, Checkbox, Title } from '@vkontakte/vkui';
import { ChipsSelect } from "@vkontakte/vkui/dist/unstable";

import { fetchTypes, setProfile } from 'store/user/actions';
import { fetchCategories } from 'store/app/actions';

import moment from 'moment';

import api from 'utils/api';

const RegPanel = ({ id, profile, router, userTypes, categories, dispatch }) => {

    useEffect(() => {
        if (profile) {
            router.navigate('events');
        }
    });
    
    useEffect(() => {
        !userTypes.length && dispatch(fetchTypes());
    }, [userTypes]);
    useEffect(() => {
        !categories.length && dispatch(fetchCategories());
    }, [categories]);

    const [form, setForm] = useState({
        lastName: '',
        firstName: '',
        middleName: '',
        phone: '',
        gender: null,
        birthday: null,
        type: null,
        categories: [],
        email: '',
    });

    const [errors, setErrors] = useState({
        lastName: false,
        firstName: false,
    });

    const setError = (field, valid) => {
        setErrors({...errors, [field]: valid});
    }
    const checkError = e => {
        const { name, value } = e.currentTarget;
        switch(name) {
            case 'lastName':
            case 'firstName':
                setError(name, value.trim().length < 2);
            break;
        }
    }

    const [categoriesValue, setCategoriesValue] = useState([]);

    const [step, setStep] = useState(1);

    const setFormValue = (name, value) => {
        setForm({...form, [name]: value });
    }

    const onChangeForm = e => {
        const { name, value } = e.currentTarget;
        setFormValue(name, value);
    }

    const isValid = (() => {
        if (step == 1) {
            return form.lastName.trim().length >= 2 && form.firstName.trim().length >= 2;
        }
    })();

    const onSubmit = e => {
        e.preventDefault();
        if (step == 1) {
            setStep(2);
        }
        else if (step == 2) {
            api.auth.reg(form).ok(data => {
                setProfile(data);
                router.navigate('events');
            }).catch(() => {
                alert('Ошибка! Пожалуйста, напишите нам');
            })
            .send();
        }
    };

    return <Panel id = { id }>
        <PanelHeader before={step == 2 ? <PanelHeaderBack onClick={e => setStep(1)} /> : ''}>Анкета регистрации</PanelHeader>
        {step == 1 && <Group>
            <FormLayout onSubmit = { onSubmit }>
                <FormItem> 
                    <Title>Основная информация</Title>
                </FormItem>
                <FormItem top = { <><span>Имя</span><span style = {{color: 'red'}}>*</span></>  } status={!errors.firstName ? "default" : "error"} bottom={errors.firstName ? "Обязательное поле" : ''}>
                    <Input type="text" name="firstName" value = { form.firstName } onChange = { onChangeForm } onBlur = { checkError } />
                </FormItem>
                <FormItem top = { <><span>Фамилия</span><span style = {{color: 'red'}}>*</span></> }  status={!errors.lastName ? "default" : "error"} bottom={errors.lastName ? "Обязательное поле" : ''}> 
                    <Input type="text" name="lastName" value = { form.lastName } onChange = { onChangeForm } onBlur = { checkError } />
                </FormItem>
                {/* <FormItem top = "Отчество">
                    <Input type="text" name="middle_name" value = { form.middle_name } onChange = { onChangeForm } />
                </FormItem> */}
                <FormItem top = "Телефон">
                    <Input type="text" name="phone" value = { form.phone } onChange = { onChangeForm } />
                </FormItem>
                <FormItem top="Пол">
                    <RadioGroup>
                        <Radio name="gender" value="MALE" onChange = { onChangeForm } checked = { form.gender == "MALE" }>Мужской</Radio>
                        <Radio name="gender" value="FEMALE" onChange = { onChangeForm } checked = { form.gender == "FEMALE" }>Женский</Radio>
                    </RadioGroup> 
                    {/* <Select placeholder="Выберите пол" options={[{value: "1", label: "Мужской"},{value: "0", label: "Женский"}]} onChange = { onChangeForm } /> */}
                </FormItem>
                <FormItem top="Дата" status={!errors.birthday ? "default" : "error"} bottom={errors.birthday ? "Обязательное поле" : ''}>
                    <DatePicker
                        min={{ day: 1, month: 1, year: 1901 }}
                        max={{ day: 1, month: 1, year: 2008 }}
                        defaultValue={{ day: 1, month: 1, year: 1990 }}
                        onDateChange={value => {
                            setFormValue('birthday', moment([value.year, value.month - 1, value.day]).toDate());
                        }}
                    />
                </FormItem>
                <FormItem>
                    <Button size="l" stretched type = "submit" disabled = { !isValid }>Далее</Button>
                </FormItem>
            </FormLayout>
        </Group>}
        {step == 2 && <Group>
            <FormLayout onSubmit = { onSubmit }>
                <FormItem> 
                    <Title>Выбор роли</Title>
                </FormItem>
                <FormItem top = "Направления">
                    <ChipsSelect options = {categories.map(c => ({value: c.id, label: c.name}))} placeholder = "Не выбраны" value = { categoriesValue } onChange = { items => {
                        setFormValue('categories', items.map(item => ({id: item.value})));
                        setCategoriesValue(items);
                    } }/>
                </FormItem>
                <FormItem top="Категория">
                    <Select placeholder="Выберите категорию" options={userTypes.map(t => ({value: t.id, label: t.name}))} onChange = { e => { setFormValue('type', {id: e.currentTarget.value}) } } />
                </FormItem>
                <FormItem>
                    <Button size="l" stretched type = "submit">Зарегистрироваться</Button>
                </FormItem>
            </FormLayout>
        </Group>}
    </Panel>
};

export default connect(({ user, app }) => ({ profile: user.profile, categories: app.categories, userTypes: user.types }))(RegPanel);