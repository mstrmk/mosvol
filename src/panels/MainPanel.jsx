import React, { useEffect } from 'react';
import {connect} from 'react-redux';
import { Panel, PanelHeader, Button, Placeholder } from '@vkontakte/vkui';
import { Icon56MessagesOutline } from '@vkontakte/icons';
import bridge from "@vkontakte/vk-bridge";

const MainPanel = ({id, vkId, isAllowed, router, profile}) => {

    const allow = () => {
        if (!isAllowed) {
            bridge.send("VKWebAppAllowMessagesFromGroup", { "group_id": Number(process.env.REACT_APP_BOT_GROUP_ID), "key": window.btoa(vkId) });
        }
    }

    useEffect(() => {
        if (isAllowed) {
            if (profile) {
                // router.navigate('events');
                router.navigate('reg');
            }
            else {
                router.navigate('reg');
            }
        }
    }, [isAllowed]);
    

    return (
        vkId && !isAllowed ? <Panel id={id}>
            <PanelHeader>Настройка бота</PanelHeader>
            <Placeholder
              icon={<Icon56MessagesOutline />}
              header="Уведомления от бота"
              action={<Button size="m" onClick = { allow }>Разрешить сообщения</Button>}
            >
              Для продолжения нужно разрешить сообщения боту
            </Placeholder>
        </Panel> : ''
    );
}

export default connect(({ vk, user }) => ({ vkId: vk.id, isAllowed: vk.isMessageAllowed, profile: user.profile }))(MainPanel);