import React, { useState, useEffect } from 'react';
import {connect} from 'react-redux';
import { Panel, PanelHeader, PanelHeaderBack, Group, FormLayout, FormItem, Input, Textarea, Select, DatePicker, RadioGroup, Radio, Button, Checkbox, Title } from '@vkontakte/vkui';
import { ChipsSelect } from "@vkontakte/vkui/dist/unstable";

import { fetchTypes, setProfile } from 'store/user/actions';
import { fetchCategories } from 'store/app/actions';

import moment from 'moment';

import api from 'utils/api';

const CreateMyEventPanel = ({ id, profile, router, categories, dispatch }) => {
    
    useEffect(() => {
        !categories.length && dispatch(fetchCategories());
    }, [categories]);

    const [form, setForm] = useState({
        name: '',
        description: '',
        claims: '',
        responsibilities: '',
        reward: '',
        address: '',
        isOnline: "true",
        ageMin: 14,
        ageMax: 99,
        tags: [],
        categories: [],
        stations: [],
        eventDays: [],
        eventPositions: [{
            "name": "test",
            "neededCount": 10,
            "dateStart": "2022-06-12T20:20:49.451Z",
            "dateEnd": "2022-06-13T20:20:49.451Z",
            "tasks": "Сделать мир лучше"
        }]
    });

    const [errors, setErrors] = useState({
        name: false,
        description: false,
    });

    const setError = (field, valid) => {
        setErrors({...errors, [field]: valid});
    }
    const checkError = e => {
        const { name, value } = e.currentTarget;
        switch(name) {
            case 'name':
            case 'description':
                setError(name, value.trim().length < 2);
            break;
        }
    }

    const [categoriesValue, setCategoriesValue] = useState([]);

    const setFormValue = (name, value) => {
        setForm({...form, [name]: value });
    }

    const onChangeForm = e => {
        const { name, value } = e.currentTarget;
        setFormValue(name, value);
    }

    const isValid = (() => {
        return form.name.trim().length >= 2 && form.description.trim().length >= 2;
    })();

    const onSubmit = e => {
        e.preventDefault();
        api.event.create(form).ok(data => {
            router.navigate('myevents');
        }).catch(() => {
            alert('Ошибка! Пожалуйста, напишите нам');
        })
        .send();
    };

    return <Panel id = { id }>
        <PanelHeader>Столичный добряк</PanelHeader>
        <FormLayout onSubmit = { onSubmit }>
            <FormItem> 
                <Title>Создать задание</Title>
            </FormItem>
            <FormItem top = { <><span>Название</span><span style = {{color: 'red'}}>*</span></>  } status={!errors.name ? "default" : "error"} bottom={errors.name ? "Обязательное поле" : ''}>
                <Input type="text" name="name" value = { form.name } onChange = { onChangeForm } onBlur = { checkError } />
            </FormItem>
            <FormItem top = { <><span>Описание</span><span style = {{color: 'red'}}>*</span></>  } status={!errors.description ? "default" : "error"} bottom={errors.description ? "Обязательное поле" : ''}>
                <Textarea type="text" name="description" value = { form.description } onChange = { onChangeForm } onBlur = { checkError } />
            </FormItem>
            <FormItem top = "Что нужно будет делать">
                <Textarea type="text" name="responsibilities" value = { form.responsibilities } onChange = { onChangeForm } />
            </FormItem>
            <FormItem top = "Требования">
                <Textarea type="text" name="claims" value = { form.claims } onChange = { onChangeForm } />
            </FormItem>
            <FormItem top = "Вознаграждение для волонтера">
                <Textarea type="text" name="reward" value = { form.reward } onChange = { onChangeForm } />
            </FormItem>
            <FormItem top="Категория">
                    <ChipsSelect options = {categories.map(c => ({value: c.id, label: c.name}))} placeholder = "Не выбраны" value = { categoriesValue } onChange = { items => {
                        setFormValue('categories', items.map(item => ({id: item.value})));
                        setCategoriesValue(items);
                    } }/>
            </FormItem>
            {/* <FormItem top = "Формат мероприятия">
                <Select options={[{value: "true", label: "Онлайн"}, {value: "false", label: "Оффлайн"}]} value = { form.isOnline } name = "isOnline" onChange = { onChangeForm } />
            </FormItem>
            <div style = {{display: form.isOnline == 'true' ? 'none' : 'block'}}>
                <FormItem top = "Адрес">
                    <Input type="text" name="address" value = { form.address } onChange = { onChangeForm } onBlur = { checkError } />
                </FormItem>
                <FormItem top = "Адрес">
                    <Input type="text" name="address" value = { form.address } onChange = { onChangeForm } onBlur = { checkError } />
                </FormItem>
            </div> */}

            <FormItem>
                <Button size="l" stretched type = "submit" disabled = { !isValid }>Создать задание</Button>
            </FormItem>
        </FormLayout>
    </Panel>
};

export default connect(({ user, app }) => ({ profile: user.profile, categories: app.categories, userTypes: user.types }))(CreateMyEventPanel);