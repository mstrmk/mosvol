import * as types from './actionTypes';

import api from 'utils/api';

export function fetchCategories() {
    return async(dispatch) => {
        api.app.fetchCategories()
            .ok((data) => {
                dispatch({
                    type: types.APP_CATEGORIES,
                    categories: data,
                });
            })
            .send();
    }
}