import Immutable from 'seamless-immutable';
import * as types from './actionTypes';

const initialState = Immutable({
    categories: [],
});

export default function reduce(state = initialState, action = {}) {
    switch (action.type) {
        case types.APP_CATEGORIES:
            return state.merge({
                categories: action.categories
            });
        default:
            return state;
    }
}