import Immutable from 'seamless-immutable';
import * as types from './actionTypes';

const initialState = Immutable({
    profile: null,
    types: [],
});

export default function reduce(state = initialState, action = {}) {
    switch (action.type) {
        case types.USER_PROFILE:
            return state.merge({
                profile: action.profile
            });
        case types.USER_TYPES:
            return state.merge({
                types: action.types
            });
        default:
            return state;
    }
}