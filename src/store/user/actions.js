import * as types from './actionTypes';

import api from 'utils/api';

export function fetchTypes() {
    return async(dispatch) => {
        api.user.fetchTypes()
            .ok((data) => {
                dispatch({
                    type: types.USER_TYPES,
                    types: data,
                });
            })
            .send();
    }
}

export function setProfile(profile) {
    return async(dispatch) => {
        dispatch({
            type: types.USER_PROFILE,
            profile
        });
    }
}