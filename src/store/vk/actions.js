import bridge from "@vkontakte/vk-bridge";
import * as types from './actionTypes';
import { USER_PROFILE as userProfileType } from '../user/actionTypes';

import api from 'utils/api';

export function initApp() {
    return async(dispatch) => {
        bridge.subscribe(e => {
            let vkEvent = e.detail;
            if (!vkEvent) {
                console.error('invalid event', e);
                return;
            }

            let type = vkEvent['type'];
            let data = vkEvent['data'];

            switch (type) {
                case 'VKWebAppUpdateInsets':
                    dispatch({
                        type: types.VK_INSETS,
                        insets: data.insets
                    });
                    break;
                case 'VKWebAppAllowMessagesFromGroupResult':
                    dispatch({
                        type: types.VK_IS_MESSAGE_ALLOWED,
                        isAllowed: data.result === true
                    });
                    break;

                case 'VKWebAppUpdateConfig':
                    dispatch({
                        type: types.VK_SCHEME,
                        scheme: data.scheme
                    });
                    break;
                default:
            }
        });

        api.auth.check()
            .ok((data) => {
                const { isAllowed, vkId, user } = data;

                dispatch({
                    type: userProfileType,
                    profile: user,
                });
                dispatch({
                    type: types.VK_ID,
                    id: vkId,
                });
                dispatch({
                    type: types.VK_IS_MESSAGE_ALLOWED,
                    isAllowed: !!isAllowed,
                });
                dispatch({
                    type: types.VK_IS_AUTH,
                    isAuth: true,
                });

                bridge.send('VKWebAppInit', { 'no_toolbar': true });
            })
            .send();

    }
}