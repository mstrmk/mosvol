import Immutable from 'seamless-immutable';
import * as types from './actionTypes';

const initialState = Immutable({
    isAuth: false,
    id: null,
    isMessageAllowed: false,
    insets: {},
    scheme: null,
    user: null,
});

export default function reduce(state = initialState, action = {}) {
    switch (action.type) {
        case types.VK_IS_AUTH:
            return state.merge({
                isAuth: action.isAuth
            });
        case types.VK_ID:
            return state.merge({
                id: action.id
            });
        case types.VK_IS_MESSAGE_ALLOWED:
            return state.merge({
                isMessageAllowed: action.isAllowed
            });
        case types.VK_INSETS:
            return state.merge({
                insets: action.insets
            });
        case types.VK_SCHEME:
            return state.merge({
                scheme: action.scheme
            });
        default:
            return state;
    }
}