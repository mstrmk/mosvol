export const VK_IS_AUTH = 'vk.isAuth';
export const VK_INSETS = 'vk.insets';
export const VK_ID = 'vk.id';
export const VK_IS_MESSAGE_ALLOWED = 'vk.isMessageAllowed';
export const VK_SCHEME = 'vk.scheme';