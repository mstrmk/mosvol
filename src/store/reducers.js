import { createStore, applyMiddleware, combineReducers } from 'redux'
import { router5Middleware, router5Reducer } from 'redux-router5'
import thunk from 'redux-thunk'

import vk from './vk/reducer';
import user from './user/reducer';
import app from './app/reducer';

export default function configureStore(router, initialState = {}) {
    const createStoreWithMiddleware = applyMiddleware(
        thunk,
        router5Middleware(router),
    )(createStore)

    const store = createStoreWithMiddleware(
        combineReducers({
            router: router5Reducer,
            vk,
            user,
            app,
        }),
        initialState
    )

    window.store = store
    return store
}