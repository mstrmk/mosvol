import auth from './modules/auth';
import user from './modules/user';
import app from './modules/app';
import event from './modules/event';
import store from './modules/store';

export default (api) => ({
    auth: auth(api),
    user: user(api),
    app: app(api),
    event: event(api),
    store: store(api),
    request: () => api.request,
})