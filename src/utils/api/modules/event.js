export default (api) => ({
    fetch(filter = {}) {
        return api.request.get('user/event', filter);
    },
    fetchOne(id) {
        return api.request.get(`user/event/${id}`);
    },
    sendRequest(eventId, comment) {
        return api.request.post(`event/user/request`, { event: { id: eventId }, comment });
    },
    fetchMy(filter = {}) {
        return api.request.get('user/event/my', filter);
    },
    create(data) {
        return api.request.post('user/event/my', data);
    }
});