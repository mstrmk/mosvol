export default (api) => ({
    fetchCategories() {
        return api.request.get('category');
    }
});