export default (api) => ({
    fetchTypes() {
        return api.request.get('user-type');
    }
});