export default (api) => ({
    check() {
        return api.request.post('user/auth');
    },
    reg(data) {
        return api.request.post('user/registration', data);
    }
});