export default (api) => ({
    fetchGoods() {
        return api.request.get('user/goods');
    },
    buyGood(id) {
        return api.request.post(`user/goods/${id}`);
    }
});