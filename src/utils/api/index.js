import Api from "./core/Api";
import axios from "axios";

import apiProvider from './api';

const api = new Api(axios);
api.setModules(apiProvider(api));

export default api;