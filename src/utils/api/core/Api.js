import ApiRequest from './ApiRequest';

export default class {

    constructor(connect, defaultHandlers = {}) {
        this._connect = connect;
        this._defaultHandlers = defaultHandlers;

        return new Proxy(this, {
            get: function($this, field) {
                if (field in $this) {
                    return $this[field];
                }

                if (field in $this._modules) {
                    return $this._modules[field];
                }

                throw (`Field [${field}] not exists in Api`);
            }
        });
    }

    _defaultHandlers = {};
    _modules = {};
    $$typeof = Symbol.for('api');

    connect(connect) {
        this._connect = connect;
        return this;
    }

    setModules(modules) {
        this._modules = modules;
    }

    formatErrors(errors) {
        let result = [];
        if (typeof errors == 'string') {
            result = [errors];
        } else {
            for (let i in errors) {
                result = result.concat(this.formatErrors(errors[i]));
            }
        }
        return result;
    }

    get request() {
        return new ApiRequest(this._connect, this._defaultHandlers);
    }
}