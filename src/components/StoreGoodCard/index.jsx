import { Card, Avatar, Button } from '@vkontakte/vkui';
import { Icon20CopyOutline } from '@vkontakte/icons';
import './style.scss';

import CoinBadge from 'components/CoinBadge';

const StoreGoodCard = ({good, buyPromo, promo, copy}) => {
    return <Card mode="shadow" className="storeGoodCard">
        <div className="storeGoodCard__content">
                <div className="storeGoodCard__body">
                    <div className = "storeGoodCard__info">
                        <div className="storeGoodCard__name">{good.name}</div>
                        <div className="storeGoodCard__partner">{good.partner.name}</div>
                        <div className="storeGoodCard__desc">{good.description}</div>
                    </div>
                    <Avatar className = "storeGoodCard__logo" src={good.partner.photo ?? 'https://sun9-21.userapi.com/s/v1/if2/PdU-MUpmivYgoa1Fdsq2pWkxW_MM250msv5-O9xdFELmlFHqFxRyybVPH-zlsUP9C0_JacwIMvRSik0FD0k01hdk.jpg?size=899x660&quality=95&type=album'} />
                </div>
                <div className = "storeGoodCard__actions">
                    {good.promoCode || promo ? <div onClick={ e => { copy(good.promoCode ?? promo) } } className = "storeGoodCard__promo"><Icon20CopyOutline width = {14} height = {14}/><span>{good.promoCode ?? promo}</span></div> : <Button size = 's' onClick = { e => { buyPromo(good.id, good.cost) }}>Купить промокод</Button>}
                    <div className="storeGoodCard__cost">
                        <CoinBadge count = { good.cost }/>
                    </div>
                </div>
        </div>
    </Card>
};

export default StoreGoodCard;