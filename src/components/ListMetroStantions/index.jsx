import './style.scss';

const ListMetroStantions = ({stantions}) => {
    return <div className = "listMetro">
        {stantions.map(s => <div className = "listMetro__item" title = { s.lines } key = { s.id }>
            <div className = "listMetro__line" style = {{backgroundColor: `#${s.hexColor}`}}></div>
            <div className = "listMetro__name">{s.name}</div>
        </div>)}
    </div>
};

export default ListMetroStantions;