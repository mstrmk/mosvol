import { Card, Caption, Title, Button } from '@vkontakte/vkui';
import ListBadges from 'components/ListBadges';
import ListMetroStantions from 'components/ListMetroStantions';
import { Icon20CheckCircleOutline } from '@vkontakte/icons';

import MyEventCardProp from './prop';
import moment from 'moment';

import './style.scss';

const MyEventCard = ({event, open}) => {
    return <Card mode="shadow" className = "myEventCard">
        <div className = "myEventCard__body">
            <Title level="1" className = "myEventCard__name" onClick = { e => {open(event.id)} }>
                {event.name}
            </Title>
            <MyEventCardProp label = "Описание">
                <span dangerouslySetInnerHTML = { {__html: event.description.length > 120 ? event.description.slice(0, 120).replace(/(\r\n|\n)/g, '<br>') + '...' : event.description.replace(/(\r\n|\n)/g, '<br>')} } />
            </MyEventCardProp>
        </div>
    </Card>
};

export default MyEventCard;