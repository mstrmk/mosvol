import { Group, Caption } from '@vkontakte/vkui';

import './style.scss';

const MyEventCardProp = ({label, children}) => {
    return <div className = "myEventCard__prop">
        <Caption level="1" className = "myEventCard__propLabel">{label}</Caption>
        {children}
    </div>
};

export default MyEventCardProp;