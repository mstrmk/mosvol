import './style.scss';
import { Icon20CommunityName, Icon56ServicesOutline, Icon56UserCircleOutline, Icon20GiftOutline } from '@vkontakte/icons';

import NavBarItem from './item';

const NavBar = ({route, router}) => {
    return <div className = "navbar"> 
        <NavBarItem icon = {<Icon20CommunityName/>} label = "Фонды и НКО" router = { router } route = { route } routeName = "funds" />
        <NavBarItem icon = {<Icon56ServicesOutline width = {20} height = {20} />} label = "Задания" router = { router } route = { route } routeName = "events" />
        <NavBarItem icon = {<Icon20GiftOutline width = {20} height = {20} />} label = "Бонусы" router = { router } route = { route } routeName = "store" />
        <NavBarItem icon = {<Icon56UserCircleOutline width = {20} height = {20} />} label = "Профиль" router = { router } route = { route } routeName = "profile" />
    </div>
};

export default NavBar;