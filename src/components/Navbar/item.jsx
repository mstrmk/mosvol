import './style.scss';

const NavBarItem = ({route, router, icon, label, routeName}) => {
    const isActive = route.name == routeName;
    return <div className = "navbar__item" onClick = { e => { if (!isActive) { router.navigate(routeName) } }}>
        <div className = {['navbar__icon', isActive ? 'active' : ''].join(' ')}>{icon}</div>
        <div className = {['navbar__label', isActive ? 'active' : ''].join(' ')}>{label}</div>
    </div>
}

export default NavBarItem;