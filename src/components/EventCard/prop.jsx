import { Group, Caption } from '@vkontakte/vkui';

import './style.scss';

const EventCardProp = ({label, children}) => {
    return <div className = "eventCard__prop">
        <Caption level="1" className = "eventCard__propLabel">{label}</Caption>
        {children}
    </div>
};

export default EventCardProp;