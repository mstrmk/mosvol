import { Card, Caption, Title, Button } from '@vkontakte/vkui';
import ListBadges from 'components/ListBadges';
import ListMetroStantions from 'components/ListMetroStantions';
import { Icon20CheckCircleOutline } from '@vkontakte/icons';

import EventCardProp from './prop';
import moment from 'moment';

import './style.scss';

const EventCard = ({event, open, sendRequest, isRequested}) => {

    const dates = ((dates) => {
        let res = [];
        if (dates.length) {
            res.push(`${moment(dates[0].day).format("D MMMM")} ${dates[0].timeStart.slice(0, -3)} - ${dates[0].timeEnd.slice(0, -3)}`);
        }
        if (dates.length > 2) {
            res.push(`...`);
        }
        if (dates.length > 1) {
            const lastDate = dates.slice(-1)[0];
            res.push(`${moment(lastDate.day).format("D MMMM")} ${lastDate.timeStart.slice(0, -3)} - ${lastDate.timeEnd.slice(0, -3)}`);
        }
        return res;
    })(event.eventDays);

    return <Card mode="shadow" className = "eventCard">
        {event.organization ? <div className = "eventCard__image">
            <img src = {event.photo ?? 'https://sun9-21.userapi.com/s/v1/if2/PdU-MUpmivYgoa1Fdsq2pWkxW_MM250msv5-O9xdFELmlFHqFxRyybVPH-zlsUP9C0_JacwIMvRSik0FD0k01hdk.jpg?size=899x660&quality=95&type=album'}/>
        </div> : ''}
        <div className = "eventCard__body">
            <Caption className = "eventCard__company" level="1">{event.organization ? event.organization.name : (event.user ? `${event.user.firstName} ${event.user.lastName.slice(0, 1)}.` : '')}</Caption>
            <Title level="1" className = "eventCard__name" onClick = { e => {open(event.id)} }>
                {event.name}
            </Title>
            {dates.length ? <EventCardProp label = "Дата и время">
                {dates.map((date, index) => <div key = {index}>{date}</div>)}
            </EventCardProp> : ''}
            <EventCardProp label = "Описание">
                <span dangerouslySetInnerHTML = { {__html: event.description.length > 120 ? event.description.slice(0, 120).replace(/(\r\n|\n)/g, '<br>') + '...' : event.description.replace(/(\r\n|\n)/g, '<br>')} } />
            </EventCardProp>
            {event.responsibilities ? <EventCardProp label = "Что нужно делать">
                <span dangerouslySetInnerHTML = { {__html: event.responsibilities.length > 120 ? event.responsibilities.slice(0, 120).replace(/(\r\n|\n)/g, '<br>') + '...' : event.responsibilities.replace(/(\r\n|\n)/g, '<br>')} }/>
            </EventCardProp> : ''}
            {event.reward ? <EventCardProp label = "Вознаграждение для Волонтеров">
                <span dangerouslySetInnerHTML = { {__html: event.reward.length > 120 ? event.reward.slice(0, 120).replace(/(\r\n|\n)/g, '<br>') + '...' : event.reward.replace(/(\r\n|\n)/g, '<br>')} } />
            </EventCardProp> : ''}
            {event.categories.length ? <EventCardProp label = "Категория">
                <ListBadges badges = { event.categories.map(c => c.name) } />
            </EventCardProp> : ''}
            {event.stantions && event.categories.length ? <EventCardProp label = "Ближайшие станции метро">
                <ListMetroStantions stantions = { event.stations } />
            </EventCardProp> : ''}
            <div className="eventCard__actions">
                {!event.isRequested && !isRequested ? <Button appearance='accent' size='l' sizeY = 'compact' stretched onClick = { e => { sendRequest(event.id) } }>Записаться волонтером</Button> : <div className = "eventCard__rqSuccess"><Icon20CheckCircleOutline/><span>Вы участвуете</span></div>}
            </div>
        </div>
    </Card>

};

export default EventCard;