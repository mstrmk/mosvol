import React, { useState } from 'react';
import { Link } from '@vkontakte/vkui';

const TextMore = ({text, maxLength, noToggle, show}) => {

    const [isShow, setIsShow] = useState(!!show);

    const toggle = e => {
        e.preventDefault();
        setIsShow(!isShow);
    };

    return <span dangerouslySetInnerHTML = { {__html: text.replace(/(\r\n|\n)/g, '<br>')} } />

    // return <span dangerouslySetInnerHTML = { {__html: text.length > maxLength ? {!isShow ? text.slice(0, maxLength).replaceAll("\r\n", '<br>') + '...' : text.replaceAll("\r\n", '<br>')} + {!noToggle ? <Link onClick = { toggle } style = { {display: 'block', marginTop: 5} }>{isShow ? 'Скрыть' : 'Показать все'}</Link> : ''} : text.replaceAll("\r\n", '<br>')} } />
}

export default TextMore;