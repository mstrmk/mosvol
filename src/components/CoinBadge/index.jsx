import './style.scss';

const CoinBadge = ({count}) => {
    return <span className = "coinBadge">{count}</span>
}

export default CoinBadge;