import { Chip } from '@vkontakte/vkui';

import './style.scss';

const ListBadges = ({badges}) => {
    return <div className = "listBadges">
         {badges.map((label, index) => <div key={index} className = "listBadges__item"><Chip removable={false}>{label}</Chip></div>)}
    </div>
};

export default ListBadges;