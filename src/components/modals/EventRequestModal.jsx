import React, { useEffect, useState } from 'react';
import {ModalRoot, ModalCard, Button, FormItem, Textarea} from '@vkontakte/vkui';
import { Icon56Hearts2CircleFillTwilight } from '@vkontakte/icons';

import api from 'utils/api';

const EventRequestModal = ({isShow, setShow, eventId, handleRequested}) => {
    const MODAL_ID = 'EventRequestModal';

    const [zIndex, setZIndex] = useState(isShow ? 9 : -1);
    const [comment, setComment] = useState('');

    useEffect(() => {
        if (!isShow) {
            setTimeout(() => {
                setZIndex(-1);
            }, 500);
        }
        else {
            setZIndex(9);
        }
        setComment('');
    }, [isShow]);

    const sendRequest = () => {
        api.event.sendRequest(eventId, comment).created(data => {
            handleRequested && handleRequested(eventId);
            setShow(false);
        }).catch(() => {
            alert('Ошибка! Пожалуйста, напишите нам');
        }).send();
    };

    return <div style = {{position: 'fixed', top: 0, left: 0, bottom: 54, zIndex, width: '100%'}}><ModalRoot activeModal={isShow ? MODAL_ID : null}>
        <ModalCard
        id={MODAL_ID}
        icon={<Icon56Hearts2CircleFillTwilight />}
        onClose = {() => { setShow(false) }}
        header="Записаться волонтером"
        subheader="Опишите дополнительную информацию о себе или о задании, чтобы организации было легче принять решение на счет Вас"
        actions={
            <Button
            size="l"
            mode="primary"
            onClick={() => { sendRequest() }}
            >
            Записаться
            </Button>
        }
        >
            <Textarea placeholder = "Дополнение к отклику" value = { comment } onChange = { e => { setComment(e.currentTarget.value) } }></Textarea>
        </ModalCard> 
    </ModalRoot></div>
};

export default EventRequestModal;