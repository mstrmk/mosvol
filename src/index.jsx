import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import { RouterProvider } from 'react-router5'
import createRouter from './router'
import App from './App';
import configureStore from './store/reducers';
import axios from 'axios';
import moment from 'moment';
import momentRuLocale from 'moment/locale/ru';

const router = createRouter()
const store = configureStore(router)

axios.defaults.baseURL = process.env.REACT_APP_API_HOST;
axios.defaults.headers.common['Authorization'] = {...window.location }.search.replace(/^\?/, '');

moment.updateLocale('ru', momentRuLocale);

router.start(() => {
    ReactDOM.render((
        <Provider store={store}>
            <RouterProvider router={router}>
                <App router={router}/>
            </RouterProvider>
        </Provider>
    ), document.getElementById('root'))
})
