

import React, { useEffect } from 'react';
import {connect} from 'react-redux';
import { ConfigProvider, AdaptivityProvider, AppRoot, Root, View } from '@vkontakte/vkui';
import { RouteNode } from 'react-router5';

import { initApp } from './store/vk/actions';

import NavBar from './components/Navbar';
import MainPanel from './panels/MainPanel.jsx';
import RegPanel from './panels/RegPanel.jsx';
import EventsPanel from './panels/EventsPanel.jsx';
import EventPanel from './panels/EventPanel';
import StorePanel from './panels/StorePanel.jsx';
import ProfilePanel from './panels/ProfilePanel.jsx';
import MyEventsPanel from './panels/MyEventsPanel';
import CreateMyEventPanel from './panels/CreateMyEventPanel';
import BotSettingsPanel from './panels/BotSettingsPanel';

import './App.css';

import '@vkontakte/vkui/dist/vkui.css';
import '@vkontakte/vkui/dist/fonts.css';

const App = ({router, route, insets, isAuth, scheme, isAllowed, dispatch}) => {

	useEffect(() => {
		!isAuth && dispatch(initApp());
	}, []);

	return (
		isAuth ? <ConfigProvider insets={insets} scheme={'vkcom_light'}>
			<AdaptivityProvider>
				<AppRoot>
					{isAllowed ? <NavBar route = {route} router = {router} /> : ''}
					<div className = "__main">
					<Root activeView="main">
						<View id="main" activePanel={route.name}>
							<MainPanel id='main' router={router} />
							<RegPanel id='reg' router={router} />
							<EventsPanel id='events' router={router} />
							<EventPanel id='event' router={router} />
							<StorePanel id='store' router={router} />
							<ProfilePanel id='profile' router={router} />
							<MyEventsPanel id='myevents' router={router} />
							<CreateMyEventPanel id='createMyevent' router={router} />
							<BotSettingsPanel id='botSettings' router={router} />
						</View>
					</Root>
					</div>
				</AppRoot>
			</AdaptivityProvider>
		</ConfigProvider> : ''
	);
}

export default connect(({ vk }) => ({isAuth: vk.isAuth, isAllowed: vk.isMessageAllowed, insets: vk.insets, scheme: vk.scheme}))(
    props => {
        return <RouteNode nodeName="">
            {({ route }) => <App route={route} {...props}/>}
        </RouteNode>
	}
);